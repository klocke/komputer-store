// import './style.css';

/*
HTML - elements.
 */
const elLaptopSelection = document.querySelector(".laptops")
const elBalanceText = document.getElementById("balance")
const elPayText = document.getElementById("pay")

const elLoanBtn = document.getElementById("loanBtn")
const elWorkBtn = document.getElementById("workBtn")
const elBankBtn = document.getElementById("bankBtn")
const elRepayLoanBtn = document.getElementById("repayLoanBtn")
const elBuyNowBtn = document.getElementById("buyNowBtn")

const elLoanOutstanding = document.getElementById("outstandingLoanDiv")
const elLoanOutstandingValue = document.getElementById("outstandingLoanValue")
const elLaptopFeatures = document.getElementById("specsLists")
const elLaptopName = document.getElementById("laptopName")
const elLaptopPreviewSection = document.getElementById("laptopPreview")
const elLaptopPrice = document.getElementById("laptopPriceDisplay")
const elLaptopPreview = document.getElementById("laptopPreviewImage")
const elLaptopDescription = document.getElementById("laptopDescriptionText")
const elLaptopFeaturesHeader = document.getElementById("laptopFeatureHeader")

/*
Set visibility to false for some buttons.
 */
elLoanOutstanding.style.visibility = "hidden" // "hidden|visible"
elRepayLoanBtn.style.visibility = "hidden"
elLaptopPreviewSection.style.visibility = "hidden"
elLaptopFeaturesHeader.style.visibility = "hidden"

/*
Event listeners
 */
elLoanBtn.addEventListener("click", getLoan);
elWorkBtn.addEventListener("click", work);
elBankBtn.addEventListener("click", depositToBank);
elLaptopSelection.addEventListener("change", laptopSelectorHandler);
elRepayLoanBtn.addEventListener("click", repayLoan);
elBuyNowBtn.addEventListener("click", buyNow);


/*
Global variables that are used.
 */
let payAmount = 0; // money in the "pay" -section
let bankBalance = 0; // money in bank (loan included)
let outstandingLoan = 0; // remaining money to pay the bank
let previousLoanRepaid = true; // keep track of last loan repaid or not.
let computerBought = false; // Disallow a second loan if a computer has not been bought
let hasTakenLoan = false;
let currentSelectedLaptop = -1; // idx
let initPage = true;
let comps = []; // list of computers fetched from api.

/*
Api endpoint
 */
const apiEndpoint = "https://noroff-komputer-store-api.herokuapp.com/"

/**
 * Function that fetches all available computers in the endpoint.
 *
 * @returns {Promise<any>}
 */
async function fetchAvailableLaptops() {
    const response = await fetch(apiEndpoint + 'computers');

    if (!response.ok) {
        throw new Error("Could not fetch the computers")
    }
    const comps = await response.json();

    comps.forEach(addComputerToSelection)
    return comps
}

/**
 * Adds computers to selection list.
 * @param comp: json response from api
 * @returns {Promise<void>}
 */
async function addComputerToSelection(comp) {
    // console.log(comp)
    const elComputerOption = document.createElement("option")
    elComputerOption.innerText = comp.title;
    elComputerOption.value = comp.id
    elLaptopSelection.appendChild(elComputerOption)
}

/**
 * Event handler for get loan button.
 * Prompts the user to
 */
function getLoan() {

    if (!canTakeLoan()) {
        return
    }

    let validLoanValue = false;
    let loanValue = -1;
    do {

        const input = prompt("How much loan do you want?")
        if (!input || !Number(input)) {
            console.log("User exit or parse error")
            return
        }


        loanValue = Number(input)
        validLoanValue = isValidLoanValue(loanValue)
    }
    while (!validLoanValue)


    outstandingLoan += loanValue
    previousLoanRepaid = false;
    hasTakenLoan = true;
    bankBalance += loanValue;

    elBalanceText.innerText = bankBalance + " kr"
    elLoanOutstandingValue.innerText = outstandingLoan + " kr";
    elLoanOutstanding.style.visibility = "visible";
    elRepayLoanBtn.style.visibility = "visible"

}

/**
 * Checks if the client can take a loan at all - regardless of value.
 * @returns {boolean}
 */
function canTakeLoan() {
    if (!computerBought && hasTakenLoan) {
        alert("You must buy a computer before taking another loan!")
        return false;
    } else if (!previousLoanRepaid) {
        alert("You must repay current loan before getting another")
        return false;
    } else if (bankBalance === 0) {
        alert("You have to earn some money to take a loan.")
        return false;
    } else return true;
}


/**
 * Helper function to determine if a value (user input) is valid.
 * @param val
 * @returns {boolean}
 */
function isValidLoanValue(val) {
    if (val > 2 * bankBalance) {
        alert("Too high loan value! Make sure you have enough money in the bank.")
        return false
    } else if (val <= 0) {
        alert("Invalid loan value")
        return false;
    } else return true
}

/**
 * Event handler for repay loan button
 */
function repayLoan() {
    let loanDeductible;
    if (outstandingLoan <= payAmount) {
        loanDeductible = outstandingLoan;
        payAmount -= loanDeductible;
        previousLoanRepaid = true;
    } else {
        loanDeductible = payAmount;
        payAmount = 0;
    }

    outstandingLoan -= loanDeductible;

    elLoanOutstandingValue.innerText = outstandingLoan + " kr";
    elPayText.innerText = payAmount + " kr"


}

/**
 * Event handler for work button
 */
function work() {
    payAmount += 100;
    elPayText.innerText = payAmount + " kr"

}

/**
 * Event handler for deposit to bank button.
 */
function depositToBank() {

    let bankDeductible; // How much to put in bank
    let loanDeductible = 0; // How much loan to repay.
    if (outstandingLoan > 0 && hasTakenLoan) {
        loanDeductible = Math.ceil(0.1 * payAmount) // Ceil instead of floor, since the bank is strict with repayment.
        if (loanDeductible > outstandingLoan) {
            loanDeductible = outstandingLoan

        }
        bankDeductible = payAmount - loanDeductible;


    } else {
        bankDeductible = payAmount;
    }
    bankBalance += bankDeductible;
    payAmount = payAmount - bankDeductible - loanDeductible;
    outstandingLoan -= loanDeductible;

    if (outstandingLoan === 0) {
        previousLoanRepaid = true;
    }

    elPayText.innerText = payAmount + " kr"
    elBalanceText.innerText = bankBalance + " kr"
    elLoanOutstandingValue.innerText = outstandingLoan + " kr"
}

/**
 * Event handler for laptop selector.
 * @param val : json from api.
 */
function laptopSelectorHandler(val) {
    const id = Number(val.target.value);
    currentSelectedLaptop = comps.find(c => Number(c.id) === id)


    // Remove "Choose a laptop" option from selection
    if (initPage) {
        elLaptopSelection.remove(0)
        elLaptopPreviewSection.style.visibility = "visible"
        elLaptopFeaturesHeader.style.visibility = "visible"

        initPage = false;
    }

    // For each new choice, the features list should not be appended to but replaced.
    elLaptopFeatures.innerHTML = "" // Dammit it took way to long to figure this one out


    // Create list of specs
    currentSelectedLaptop.specs.forEach(c => {

            const htmlLiElement = document.createElement("li");
            htmlLiElement.innerText = String(c);

            elLaptopFeatures.appendChild(htmlLiElement);

        }
    )


    elLaptopName.innerText = currentSelectedLaptop.title;
    elLaptopPrice.innerText = currentSelectedLaptop.price + " kr";
    elLaptopDescription.innerText = currentSelectedLaptop.description;

    updateImage();


}

/**
 * Function to update the image preview of a laptop.
 * A default image is shown if the image link should be broken for _some_ reason.
 */
function updateImage() {
    elLaptopPreview.innerHTML = ""
    const img = document.createElement("img")
    img.src = apiEndpoint + currentSelectedLaptop.image
    img.alt = "Image of computer";
    img.onerror = () => {
        img.src = "assets/missing.jpg";
        img.onerror = null;

    }
    elLaptopPreview.appendChild(img)
}

/**
 * Event handler for buy now button
 */
function buyNow() {
    if (currentSelectedLaptop === -1) {
        alert("Select a laptop first!")
        return
    }


    let price = currentSelectedLaptop.price
    if (bankBalance >= price) {
        bankBalance -= price
        elBalanceText.innerText = bankBalance + " kr";
        computerBought = true;
        alert(`Congratulations, you are now the owner of: "${currentSelectedLaptop.title}"`)
    } else {
        alert("Not enough money in the bank :(")
    }

}

/**
 * I actually do not know what this syntax is,
 * but it has been suggested on several stackexchange posts.
 * My guess is that you need a function call to an async function in the file?
 */
(async function () {
    comps = await fetchAvailableLaptops();
})()
